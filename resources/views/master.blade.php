<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.meta')
    @yield('css_custom')
</head>
<body>
    @include('layout.header')
    <!-- End Header -->
              @yield('content')
    <!-- content Footer -->
    <!-- Footer -->
      @include('layout.footer')
    <!-- End Footer -->
    @yield('scripts_custom')
</body>
</html>