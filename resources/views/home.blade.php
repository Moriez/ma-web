@extends('master')
@section('css_custom')

@endsection
@section('content')

<div class="carousel">
<!-- Slider -->
<div id="slider">
  <div class="slides">
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
          <h1>Lorem ipsum dolor</h1>
          <h2>Nam ultrices pellentesque facilisis. In semper tellus mollis nisl pulvinar vitae vulputate lorem consequat. Fusce odio tortor, pretium sit amet auctor ut, ultrices vel nibh.</h2>
        </div>
      </div>
      <div class="image">
        <img src="{{asset('img/aa.png')}}">
      </div>
    </div>
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
          <h1>Lorem ipsum dolor</h1>
          <h2>Nam ultrices pellentesque facilisis. In semper tellus mollis nisl pulvinar vitae vulputate lorem consequat. Fusce odio tortor, pretium sit amet auctor ut, ultrices vel nibh.</h2>
        </div>
      </div>
      <div class="image">
        <img src="{{asset('img/aa.png')}}">
      </div>
    </div>
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
          <h1>Lorem ipsum dolor</h1>
          <h2>Nam ultrices pellentesque facilisis. In semper tellus mollis nisl pulvinar vitae vulputate lorem consequat. Fusce odio tortor, pretium sit amet auctor ut, ultrices vel nibh.</h2>
        </div>
      </div>
      <div class="image">
        <img src="{{asset('img/aa.png')}}">
      </div>
    </div>
    <div class="slider">
      <div class="legend"></div>
      <div class="content">
        <div class="content-txt">
          <h1>Lorem ipsum dolor</h1>
          <h2>Nam ultrices pellentesque facilisis. In semper tellus mollis nisl pulvinar vitae vulputate lorem consequat. Fusce odio tortor, pretium sit amet auctor ut, ultrices vel nibh.</h2>
        </div>
      </div>
      <div class="image">
        <img src="{{asset('img/aa.png')}}">
      </div>
    </div>
  </div>
  <div class="switch">
    <ul>
      <li>
        <div class="on"></div>
      </li>
      <li></li>
      <li></li>
      <li></li>
    </ul>
  </div>
</div>
{{--  end slider  --}}
</div>
<div class="main-contain">
<div class="grid-container">
  <div class="grid-item item1">
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id cum minus sit culpa ab 
    placeat veritatis autem fugiat libero ducimus, recusandae cupiditate modi deserunt quibusdam ipsum neque. Error, dolore voluptatum.
</p>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id cum minus sit culpa ab 
    placeat veritatis autem fugiat libero ducimus, recusandae cupiditate modi deserunt quibusdam ipsum neque. Error, dolore voluptatum.
</p>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id cum minus sit culpa ab 
    placeat veritatis autem fugiat libero ducimus, recusandae cupiditate modi deserunt quibusdam ipsum neque. Error, dolore voluptatum.
</p>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id cum minus sit culpa ab 
    placeat veritatis autem fugiat libero ducimus, recusandae cupiditate modi deserunt quibusdam ipsum neque. Error, dolore voluptatum.
</p>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id cum minus sit culpa ab 
    placeat veritatis autem fugiat libero ducimus, recusandae cupiditate modi deserunt quibusdam ipsum neque. Error, dolore voluptatum.
</p>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id cum minus sit culpa ab 
    placeat veritatis autem fugiat libero ducimus, recusandae cupiditate modi deserunt quibusdam ipsum neque. Error, dolore voluptatum.
</p>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id cum minus sit culpa ab 
    placeat veritatis autem fugiat libero ducimus, recusandae cupiditate modi deserunt quibusdam ipsum neque. Error, dolore voluptatum.
</p>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id cum minus sit culpa ab 
    placeat veritatis autem fugiat libero ducimus, recusandae cupiditate modi deserunt quibusdam ipsum neque. Error, dolore voluptatum.
</p>
  </div>
  <div class="grid-item item2">
      <h5>testimoni alumni</h5>
  <div class="blog-card">
    <div class="meta">
      <div class="photo" style="background-image: url(/img/moriez.jpg)"></div>
      <ul class="details">
        <li class="author"><a href="#">John Doe</a></li>
        <li class="date">Aug. 24, 2015</li>
        <li class="tags">
          <ul>
            <li><a href="#">Learn</a></li>
            <li><a href="#">Code</a></li>
            <li><a href="#">HTML</a></li>
            <li><a href="#">CSS</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="description">
      <h1>Learning to Code</h1>
      <h2>Opening a door to the future</h2>
      <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati</p>
      <p class="read-more">
        <a href="#">Read More</a>
      </p>
    </div>
  </div>
  <div class="blog-card alt">
    <div class="meta">
      <div class="photo" style="background-image: url(/img/AB.jpg)"></div>
      <ul class="details">
        <li class="author"><a href="#">Jane Doe</a></li>
        <li class="date">July. 15, 2015</li>
        <li class="tags">
          <ul>
            <li><a href="#">Learn</a></li>
            <li><a href="#">Code</a></li>
            <li><a href="#">JavaScript</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="description">
      <h1>Mastering the Language</h1>
      <h2>Java is not the same as JavaScript</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati</p>
      <p class="read-more">
        <a href="#">Read More</a>
      </p>
    </div>
  </div>
  </div>
  <div class="grid-item item3">
    <h5>seputar madrasah</h5>
    <button class="accordion">Section 1</button>
        <div class="panel">
            <p>Lorem ipsum...</p>
        </div>

    <button class="accordion">Section 2</button>
        <div class="panel">
        <p>Lorem ipsum...</p>
        </div>

    <button class="accordion">Section 3</button>
        <div class="panel">
            <p>Lorem ipsum...</p>
        </div>
  </div>
</div>
</div>

@endsection