<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::prefix('{lang?}')->group(function () {
    Route::get('/', 'index_controller@index')->name('home');
    Route::get('/about', 'about_controller@index')->name('about');
    // Route::get('/pivacypolice', 'privpolController@index')->name('privpol');
    // Route::get('/respongame', 'respongameController@index')->name('respon');
    // Route::get('/termsandcondition', 'termscomndController@index')->name('termscond');
// });